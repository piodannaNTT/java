package org.example;

public class ValidatorePassword {
    public static boolean convalida(String password){
        boolean numero = false;
        boolean maiuscola = false;
        boolean minuscola = false;
        if(password.length() < 8)
            return false;
        char c;
        for(int i = 0; i<password.length(); i++){
            c = password.charAt(i);
            if(Character.isDigit(c))
                numero = true;
            if (Character.isUpperCase(c))
                maiuscola = true;
            if(Character.isLowerCase(c))
                minuscola = true;
        }
        if(numero && maiuscola && minuscola)
            return true;
        else
            return false;
    }
}
