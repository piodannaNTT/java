package org.example;

public class Ale {
    public static boolean valida(String s){
        boolean contNumero = false;
        boolean contLower = false;
        boolean contUpper = false;
        for (int i = 0; i<s.length(); i++){
            if (Character.isDigit(s.charAt(i))){
                contNumero = true;}
            if(Character.isLowerCase(s.charAt(i))){
                contLower = true;
            }
            if (Character.isUpperCase(s.charAt(i))){
                contUpper = true;
            }

        }
        if ((s.length() >= 8) && contNumero == true && contUpper == true && contLower == true){
            System.out.println("password accettata");
            return true;
        }
        else{
            System.out.println("password rifiutata");
            return false;
        }
    }
}
