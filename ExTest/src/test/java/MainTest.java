import org.example.Main;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void testPariDispari() {
        assertFalse(Main.pariDispari(3,5));
        assertTrue(Main.pariDispari(8, 6));
        assertTrue(Main.pariDispari(6, 8));
    }
}