package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Ordine {
    public enum Stato{
        IN_ATTESA,
        SPEDITO,
        CONSEGNATO
    }
    private String idOrdine;
    private HashMap<Prodotto, Integer> prodottiOrdinati;
    private Stato stato;
    private double totaleOrdine;

    public Ordine(String idOrdine, Stato stato, double totaleOrdine) {
        this.idOrdine = idOrdine;
        this.prodottiOrdinati = new HashMap<>();
        this.stato = stato;
        this.totaleOrdine = totaleOrdine;
    }

    public Ordine(String idOrdine, HashMap<Prodotto, Integer> prodottiOrdinati, Stato stato, double totaleOrdine) {
        this.idOrdine = idOrdine;
        this.prodottiOrdinati = prodottiOrdinati;
        this.stato = stato;
        this.totaleOrdine = totaleOrdine;
    }

    public String getIdOrdine() {
        return idOrdine;
    }

    public void setIdOrdine(String idOrdine) {
        this.idOrdine = idOrdine;
    }


    public Stato getStato() {
        return stato;
    }

    public void setStato(Stato stato) {
        this.stato = stato;
    }

    public double getTotaleOrdine() {
        return totaleOrdine;
    }

    public void setTotaleOrdine(double totaleOrdine) {
        this.totaleOrdine = totaleOrdine;
    }

    public HashMap<Prodotto, Integer> getProdottiOrdinati() {
        return prodottiOrdinati;
    }

    public void setProdottiOrdinati(HashMap<Prodotto, Integer> prodottiOrdinati) {
        this.prodottiOrdinati = prodottiOrdinati;
    }

    public boolean aggiungiProdotto(Prodotto prodottoOrdinato, int quantitaOrdinata){
        if(prodottoOrdinato != null){
            this.prodottiOrdinati.put(prodottoOrdinato, quantitaOrdinata);
            return true;
        }
        return false;
    }

    public boolean rimuoviProdotto(Prodotto prodotto){
        if(!this.prodottiOrdinati.isEmpty()) {
            this.prodottiOrdinati.remove(prodotto);
            return true;
        }
        return false;
    }

    public double calcolaTotaleOrdine(){
        double totale = 0;
        for(Map.Entry<Prodotto, Integer> prodottoEntry : this.prodottiOrdinati.entrySet()){
            totale += prodottoEntry.getKey().getPrezzo();
        }
        return totale;
    }

    public boolean aggiornaStatoOrdine(Stato newStato){
        if(!this.stato.equals(newStato)){
            setStato(newStato);
            return true;
        }
        return false;
    }
}
