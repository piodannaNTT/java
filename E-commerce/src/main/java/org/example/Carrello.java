package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Carrello {
    private HashMap<Prodotto, Integer> prodottiScelti;
    private Promozione promozione;

    public Carrello() {
        this.prodottiScelti = new HashMap<>();
    }

    public Carrello(HashMap<Prodotto, Integer> prodottiScelti, Promozione promozione) {
        this.prodottiScelti = prodottiScelti;
        this.promozione = promozione;
    }

    public HashMap<Prodotto, Integer> getProdottiScelti() {
        return prodottiScelti;
    }

    public void setProdottiScelti(HashMap<Prodotto, Integer> prodottiScelti) {
        this.prodottiScelti = prodottiScelti;
    }

    public Promozione getPromozione() {
        return promozione;
    }

    public void setPromozione(Promozione promozione) {
        this.promozione = promozione;
    }

    public boolean aggiungiProdotto(Prodotto prodotto, int quantita){
        if(prodotto != null && quantita > 0){
            this.prodottiScelti.put(prodotto, quantita);
            return true;
        }
        return false;
    }

    public boolean rimuoviProdotto(Prodotto prodotto){
        if(prodotto != null && !this.prodottiScelti.isEmpty()) {
            this.prodottiScelti.remove(prodotto);
            return true;
        }
        return false;
    }

    public void applicaPromozione(Promozione promozione){
        if(promozione != null){
            setPromozione(promozione);
        }
    }

    public double totale(){
        double totale = 0;
        for(Map.Entry<Prodotto, Integer> prodotto : this.prodottiScelti.entrySet()){
            totale += prodotto.getKey().getPrezzo() * prodotto.getValue();
        }
        return totale;
    }

    public double totaleScontato(){
        double totaleScontato = 0;
        if (!this.prodottiScelti.isEmpty() && this.promozione != null) {
            totaleScontato = totale() - ((totale()*this.promozione.getPrecentualeSconto())/100);
        }
        return totaleScontato;
    }
}
