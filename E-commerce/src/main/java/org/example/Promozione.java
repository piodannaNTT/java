package org.example;

public class Promozione {
    private String codicePromozione;
    private String descrizione;
    private double precentualeSconto;

    public Promozione(String codicePromozione, String descrizione, double precentualeSconto) {
        this.codicePromozione = codicePromozione;
        this.descrizione = descrizione;
        this.precentualeSconto = precentualeSconto;
    }

    public String getCodicePromozione() {
        return codicePromozione;
    }

    public void setCodicePromozione(String codicePromozione) {
        this.codicePromozione = codicePromozione;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public double getPrecentualeSconto() {
        return precentualeSconto;
    }

    public void setPrecentualeSconto(double precentualeSconto) {
        this.precentualeSconto = precentualeSconto;
    }

    public double applicaSconto(Ordine ordine){
        double sconto = 0;
        if(!ordine.getProdottiOrdinati().isEmpty()){
            sconto = (ordine.calcolaTotaleOrdine()*this.precentualeSconto)/100;
            return ordine.calcolaTotaleOrdine()-sconto;
        }
        return sconto;
    }
}
