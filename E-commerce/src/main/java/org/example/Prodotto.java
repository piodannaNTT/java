package org.example;

public class Prodotto {
    private String id;
    private String nome;
    private double prezzo;
    private int quantitàInventario;
    public Prodotto(String id, String nome, double prezzo, int quantitàInventario) {
        this.id = id;
        this.nome = nome;
        this.prezzo = prezzo;
        this.quantitàInventario = quantitàInventario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public int getQuantitàInventario() {
        return quantitàInventario;
    }

    public void setQuantitàInventario(int quantitàInventario) {
        this.quantitàInventario = quantitàInventario;
    }

    public boolean disponibilitàInventario(){
        if(getQuantitàInventario() >= 0)
            return true;
        return false;
    }
}
