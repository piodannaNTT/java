package org.example;

import java.util.ArrayList;

public class Repository {
    private String nome;
    private ArrayList<Commit> commits;

    public Repository(String nome){
        this.nome = nome;
        this.commits = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Commit> getCommits() {
        return commits;
    }

    public void setCommits(ArrayList<Commit> commits) {
        this.commits = commits;
    }

    public boolean aggiungiCommit(Commit commit){
        if(commit != null){
            return this.commits.add(commit);
        }
        return false;
    }

    public Commit ultimoCommit(){
        if(this.commits.size() != 0){
            return this.commits.get(this.commits.size()-1);
        }
        return null;
    }

    public int numeroDiCommits(){
        return this.commits.size();
    }
}
