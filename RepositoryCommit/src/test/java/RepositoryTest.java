import org.example.Commit;
import org.example.Repository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryTest {

    Repository repository;
    Commit commit;
    Commit nuovo;
    @BeforeEach
    void setUp(){
        repository = new Repository("TestRepo");
        commit = new Commit("vghbj42", "Funziona Rick");
        nuovo = new Commit("24f3cv325", "Prova");
    }

    @Test
    void aggiungiCommit() {
        assertEquals(0, repository.numeroDiCommits());
        assertTrue(repository.aggiungiCommit(commit));
        assertEquals(1, repository.numeroDiCommits());
    }

    @Test
    void ultimoCommit() {
        repository.aggiungiCommit(commit);
        assertEquals(commit, repository.ultimoCommit());

        assertTrue(repository.aggiungiCommit(nuovo));
        assertEquals(nuovo, repository.ultimoCommit());
    }

    @Test
    void numeroDiCommits() {
        assertEquals(0, repository.numeroDiCommits());
        repository.aggiungiCommit(commit);
        repository.aggiungiCommit(nuovo);
        assertEquals(2, repository.numeroDiCommits());
        assertNotEquals(4, repository.numeroDiCommits());
    }
}