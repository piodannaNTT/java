import org.example.Commit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommitTest {

    @Test
    void verificaCostruttore(){
        Commit commit1 = new Commit("fv324rf", "Commit di test");
        assertNotNull(commit1);
        Commit commit2 = new Commit("", "");
        assertNotNull(commit2);
        Commit commit3 = new Commit(null, null);
        assertNotNull(commit3);
    }
}

