package org.example;
import java.util.Date;

public class LibroStampato extends Libro{
    private int numeroPagine;
    public LibroStampato(String titolo, String autore, String data_pubblicazione, int numeroPagine) {
        super(titolo, autore, data_pubblicazione);
        this.numeroPagine = numeroPagine;
    }

    public int getNumeroPagine() {
        return numeroPagine;
    }

    public void setNumeroPagine(int numeroPagine) {
        this.numeroPagine = numeroPagine;
    }
}
