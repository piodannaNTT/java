package org.example;
import java.util.ArrayList;

public class Biblioteca {
    private ArrayList<Libro> libri;

    public Biblioteca(){
        this.libri = new ArrayList<>();
    }

    public ArrayList<Libro> getLibri() {
        return libri;
    }

    public void setLibri(ArrayList<Libro> libri) {
        this.libri = libri;
    }

    public boolean addLibro(Libro libro){
        if(libro != null){
            this.libri.add(libro);
            return true;
        }else{
            System.out.println("Dati inseriti non corretti");
            return false;
        }
    }

    public Libro searchLibro(String titolo){
        for(Libro libro : this.libri){
            if(libro.titolo.equals(titolo)){
                System.out.println("Libro trovato!\n");
                return libro;
            }
        }
        System.out.println("Libro non presente in biblioteca\n");
        return null;
    }

    public int calcNumPagTotaliLibriStampati(){
        int pagTotali = 0;
        for(Libro libro : this.libri){
            if(libro instanceof LibroStampato)
                pagTotali+= ((LibroStampato) libro).getNumeroPagine();
        }
        return pagTotali;
    }

    public void visualizzaLibri(){
        for(Libro libro : this.libri){
            if(libro instanceof LibroStampato){
                System.out.println("Tipologia - Libro stampato");
                libro.infoLibro();
                System.out.println("Numero di pagine: "+((LibroStampato) libro).getNumeroPagine()+"\n");
            } else if (libro instanceof LibroDigitale) {
                System.out.println("Tipologia - Libro digitale");
                libro.infoLibro();
                System.out.println("Tipo di formato: "+((LibroDigitale) libro).getFormatoDigitale()+"\n");
            }
        }
    }
}
