import org.example.Libro;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LibroTest {

    Libro libro;
    @BeforeEach
    void setUp() {
        libro = new Libro("I segreti di JavaCat", "Sofia Di Blasi", "22/05/2023");
    }

    @Test
    void verificaProprietà() {
        assertAll("Proprietà Libro",
                () -> assertNotNull(libro),
                () -> assertNotEquals("", libro.getAutore()),
                () -> assertNotEquals("", libro.getTitolo()),
                () -> assertNotNull(libro.getData_pubblicazione())
        );
    }
}