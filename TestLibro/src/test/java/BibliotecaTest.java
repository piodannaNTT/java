import org.example.Biblioteca;
import org.example.Libro;
import org.example.LibroDigitale;
import org.example.LibroStampato;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BibliotecaTest {

    Biblioteca biblioteca = new Biblioteca();
    LibroStampato libroStampato1 = new LibroStampato("Interstellar", "Jack Ryan", "20/09/1990", 540);
    LibroStampato libroStampato2 = new LibroStampato("Non mi piace leggere", "Piuccio", "05/04/2024", 33);
    LibroDigitale libroDigitale1 = new LibroDigitale("Harry Potter", "J.K.Rowling", "12/05/1995", "MOBI");


    @Test
    void addLibro() {
        assertEquals(0, biblioteca.getLibri().size());
        assertTrue(biblioteca.addLibro(libroStampato1));
        assertTrue(biblioteca.addLibro(libroStampato2));
        assertTrue(biblioteca.addLibro(libroDigitale1));
        assertEquals(3, biblioteca.getLibri().size());
        assertFalse(biblioteca.addLibro(null));
    }

    @Test
    void searchLibro() {
        biblioteca.addLibro(libroDigitale1);
        assertNotNull(biblioteca.searchLibro("Harry Potter"));
        assertNull(biblioteca.searchLibro("dfgfd"));
    }
}