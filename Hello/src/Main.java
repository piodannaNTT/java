//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        String string = "Ciao mamma come stai";
        char c1 = 'i';
        char c2 = 'a';

        if(contaOccorrenze(string, c1) > contaOccorrenze(string, c2)){
            System.out.println("Sono presenti più occorrenze del primo carattare '"+c1+"'");
        } else if (contaOccorrenze(string, c1) < contaOccorrenze(string, c2)) {
            System.out.println("Sono presenti più occorrenze del secondo carattare '"+c2+"'");
        } else if (contaOccorrenze(string, c1) == contaOccorrenze(string, c2)) {
            System.out.println("Le occorrenze dei due carattere sono uguali");
        }


    }
    public static int contaOccorrenze(String string, char c){
        int count = 0;
        for(int i = 0; i<string.length(); i++){
            if(string.charAt(i) == c){
                count++;
            }
        }
        return count;
    }
}

/*

public static void main(String[] args) {
        if(controlla("Ciao", 'c')){
            System.out.println("Il carattere è presente nella stringa");
        }else{
            System.out.println("Il carattere non è presente nella stringa");
        }
    }


 public static void main(String[] args) {
        String string = "Ciao mamma come stai";
        char c = 'a';
        System.out.println("Sono presenti "+contaOccorrenze(string, c)+ " occorrenze del carattare '"+c+"'");
    }


public static boolean controlla(String string, char c){
        for(int i = 0; i<string.length(); i++){
            if(string.charAt(i) == c){
                return true;
            }
        }
        return false;
    }



 */


