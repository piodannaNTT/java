package it.esercizi.Scuola;

public class TestStudenti {
    public static void main(String[] args){
        StudenteInformatica studenteInformatica = new StudenteInformatica("Pio D'Anna", 863001624, "Ingegneria Informatica");
        StudenteMatematica studenteMatematica = new StudenteMatematica("Lorenzo Abate", 872313445, "Matemarica analitica");
        studenteInformatica.stampaInfo();
        studenteMatematica.stampaInfo();
    }
}
