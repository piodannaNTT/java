package it.esercizi.Scuola;

public class StudenteMatematica extends Studente{
    private String indirizzoDiStudio;
    public StudenteMatematica(String nome, int matricola, String indirizzoDiStudio) {
        super(nome, matricola);
        this.indirizzoDiStudio = indirizzoDiStudio;
    }

    @Override
    public void stampaInfo() {
        super.stampaInfo();
        System.out.println("Indirizzo di studio: "+this.indirizzoDiStudio+"\n");
    }
}
