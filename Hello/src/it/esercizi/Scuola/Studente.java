package it.esercizi.Scuola;

public class Studente {
    private String nome;
    private int matricola;

    public Studente(String nome, int matricola){
        this.nome = nome;
        this.matricola = matricola;
    }

    public void stampaInfo(){
        System.out.println("Nome: "+this.nome+"\nMatricola: "+this.matricola);
    }
}
