package it.esercizi;

import java.util.Scanner;

public class es1 {
    public  static void main(String[] args){
        Scanner in = new Scanner(System.in);
        char carattere;
        String frase;
        System.out.println("Inserisci il testo");
        frase = in.nextLine();
        System.out.println("Inserisci il carattere da ricercare");
        carattere = in.nextLine().charAt(0);

        boolean result = cerca(frase, carattere);
        System.out.println("Il risultato della ricerca è: "+result);
    }

    public static boolean cerca(String frase, char carattere){
        for(int i = 0; i < frase.length(); i++){
            if(frase.charAt(i) == carattere) {
                return true;
            }
        }
        return false;

    }
}
