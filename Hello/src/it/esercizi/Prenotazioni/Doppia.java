package it.esercizi.Prenotazioni;

public class Doppia extends Camera{
    public Doppia(int numeroCamera, boolean statoPrenotazione) {
        super(numeroCamera, statoPrenotazione);
    }


    public void tostring() {
        System.out.println("Camera doppia n°: "+this.numeroCamera+"\nStato della prenotazione: "+((this.statoPrenotazione) ? "Libera" : "Occupata")+"\n");
    }
}
