package it.esercizi.Prenotazioni;

public class Singola extends Camera{
    public Singola(int numeroCamera, boolean statoPrenotazione) {
        super(numeroCamera, statoPrenotazione);
    }

    @Override
    public void tostring() {
        System.out.println("Camera singola n°: "+this.numeroCamera+"\nStato della prenotazione: "+((this.statoPrenotazione) ? "Libera" : "Occupata")+"\n");
    }
}
