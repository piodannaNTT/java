package it.esercizi.Prenotazioni;

public class Suite extends Camera{
    public Suite(int numeroCamera, boolean statoPrenotazione) {
        super(numeroCamera, statoPrenotazione);
    }

    @Override
    public void tostring() {
        System.out.println("Suite n°: "+this.numeroCamera+"\nStato della prenotazione: "+((this.statoPrenotazione) ? "Libera" : "Occupata")+"\n");
    }
}
