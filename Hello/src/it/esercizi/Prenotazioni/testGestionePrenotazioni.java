package it.esercizi.Prenotazioni;

public class testGestionePrenotazioni {
    public static void main(String[] args){
        GestionePrenotazioni sistema = new GestionePrenotazioni();
        Singola singola1 = new Singola(124, true);
        Doppia doppia1 = new Doppia(432, false);
        Suite suite1 = new Suite(674, true);
        Singola singola2 = new Singola(125, false);
        Singola singola3 = new Singola(126, true);

        sistema.aggiungiCamera(singola1);
        sistema.aggiungiCamera(singola2);
        sistema.aggiungiCamera(singola3);
        sistema.aggiungiCamera(doppia1);
        sistema.aggiungiCamera(suite1);

        sistema.rimuoviCamera(125);

        Camera camera = sistema.cercaCamera(125);
        if(camera != null)
            camera.tostring();

        sistema.aggiornaStatoPrenotazione(674, false);

        sistema.visualizzaCamere();
        sistema.visualizzaCamereDisponibili();
    }
}
