package it.esercizi.Prenotazioni;

import java.util.HashMap;
import java.util.Map;

public class GestionePrenotazioni {
    private HashMap<Integer, Camera> camere;

    public GestionePrenotazioni(){
        this.camere = new HashMap<>();
    }

    public boolean aggiungiCamera(Camera camera){
        if(camera != null){
            this.camere.put(camera.numeroCamera, camera);
            return true;
        }
        System.out.println("Inserimento non riuscito");
        return false;
    }

    public boolean rimuoviCamera(int numeroCamera){
        if(this.camere.containsKey(numeroCamera)){
            this.camere.remove(numeroCamera);
            return true;
        }
        System.out.println("Numero di camera non presente tra quelle dispobili per la rimozione");
        return false;
    }

    public Camera cercaCamera(int numeroCamera){
        if(this.camere.containsKey(numeroCamera)){
            for(Map.Entry<Integer, Camera> entry : this.camere.entrySet()){
                if(entry.getKey() == numeroCamera){
                    return entry.getValue();
                }
            }
        }
        System.out.println("\nCamera n°:"+numeroCamera+" non presente tra quelle dispobili\n");
        return null;
    }

    public boolean aggiornaStatoPrenotazione(int numeroCamera, boolean prenotata){
        if(this.camere.containsKey(numeroCamera)){
            for(Map.Entry<Integer, Camera> entry : this.camere.entrySet()){
                if(entry.getKey() == numeroCamera){
                    entry.getValue().setStatoPrenotazione(prenotata);
                    return true;
                }
            }
        }
        System.out.println("\nCamera n°:"+numeroCamera+" non presente tra quelle dispobili\n");
        return false;
    }

    public void visualizzaCamere(){
        System.out.println("Camere nella struttura\n--------------------");
        for(Map.Entry<Integer, Camera> entry : this.camere.entrySet()){
                entry.getValue().tostring();
        }
        System.out.println("--------------------");
    }
    public void visualizzaCamereDisponibili(){
        System.out.println("Camere disponibili\n--------------------");
        for(Map.Entry<Integer, Camera> entry : this.camere.entrySet()){
            if(entry.getValue().isStatoPrenotazione() == true){
                entry.getValue().tostring();
            }
        }
        System.out.println("--------------------");
    }
}
