package it.esercizi.FormeGeometriche;

public class FormaGeometrica {
    float altezza;
    float larghezza;
    float profondita;
    public FormaGeometrica(){
    }

    public FormaGeometrica(float altezza, float larghezza, float profondita) {
        this.altezza = altezza;
        this.larghezza = larghezza;
        this.profondita = profondita;
    }

    public float getAltezza() {
        return altezza;
    }

    public float getLarghezza() {
        return larghezza;
    }

    public float getProfondita() {
        return profondita;
    }

    public void setAltezza(float altezza) {
        this.altezza = altezza;
    }

    public void setLarghezza(float larghezza) {
        this.larghezza = larghezza;
    }

    public void setProfondita(float profondita) {
        this.profondita = profondita;
    }

    public float calcolaVolume(){
        return this.altezza * this.larghezza * this.profondita;
    }
}
