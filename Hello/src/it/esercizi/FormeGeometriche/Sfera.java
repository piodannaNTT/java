package it.esercizi.FormeGeometriche;

public class Sfera {
    private float raggio;
    public Sfera(float raggio){
        super();
        this.raggio = raggio;
    }

    public float getRaggio() {
        return raggio;
    }

    public void setRaggio(float raggio) {
        this.raggio = raggio;
    }

    public float calcolaVolume(){
        return (float) ((4 * Math.PI * this.raggio * 3)/3);
    }
}
