package it.esercizi.FormeGeometriche;

public class Cilindro extends FormaGeometrica{
    float raggio;

    public Cilindro(float altezza, float raggio) {
        super();
        this.altezza = altezza;
        this.raggio = raggio;
    }

    public float getRaggio() {
        return raggio;
    }

    public void setRaggio(float raggio) {
        this.raggio = raggio;
    }

    @Override
    public float calcolaVolume() {
        return (float) (Math.PI*(Math.pow(this.raggio, 2))*this.altezza);
    }
}
