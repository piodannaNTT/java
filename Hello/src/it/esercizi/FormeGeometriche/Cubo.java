package it.esercizi.FormeGeometriche;

public class Cubo extends FormaGeometrica{
    private float lato;
    public Cubo(float lato) {
        super();
        this.lato = lato;
    }

    public float getLato() {
        return lato;
    }

    public void setLato(float lato) {
        this.lato = lato;
    }

    @Override
    public float calcolaVolume(){
        return (float) Math.pow(this.lato, 3);
    }
}
