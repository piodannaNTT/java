package it.esercizi.banca;

public class TestConto {
    public static void main(String[] args){
        ContoBancario contoBancario = new ContoBancario("Pio D'Anna", "IT32R233423900000232", 0);
        contoBancario.versa(1000);
        contoBancario.preleva(500);
        contoBancario.preleva(600);
        System.out.println("Sul conto corrente sono presenti: "+contoBancario.getSaldo()+"€");
    }
}
