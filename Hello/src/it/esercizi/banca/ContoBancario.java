package it.esercizi.banca;

public class ContoBancario {
    String intestatario;
    double saldo;
    String IBAN;

    public ContoBancario(){
    }

    public ContoBancario(String intestatario, String IBAN, double saldo){
        this.intestatario = intestatario;
        this.IBAN = IBAN;
        this.saldo = saldo;
    }

    public String getIntestatario() {
        return intestatario;
    }

    public String getIBAN() {
        return IBAN;
    }

    public double getSaldo() {
        return saldo;
    }

    public void versa(double quota){
        this.saldo+=quota;
    }
    public boolean preleva(double quota){
        if(this.saldo > quota){
            this.saldo-=quota;
            return true;
        }else {
            System.out.println("Operazione non consentita, saldo insufficente");
            return false;
        }

    }
}
