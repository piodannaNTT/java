package it.esercizi.AziendaNoleggio;

import java.text.DecimalFormat;

public class Auto {
    private String targa;
    private String marca;
    private String modello;
    private double prezzo;
    public Auto(String targa, String marca, String modello, double prezzo){
        this.targa = targa;
        this.marca = marca;
        this.modello = modello;
        this.prezzo = prezzo;
    }

    public String getTarga() {
        return targa;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModello() {
        return modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public void infoAuto(){
        //NumberFormat formatter = new DecimalFormat("#0.00");
        System.out.println("Targa: "+this.targa+
                "\nMarca: "+this.marca+"\nModello: "+this.modello+
                "\nPrezzo noleggio: "+String.format("%.2f", this.prezzo)+"€\n");
    }
}
