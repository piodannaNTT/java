package it.esercizi.AziendaNoleggio;

import java.util.HashSet;
import java.util.Iterator;

public class AziendaNoleggio {
    private HashSet<Auto> parcoAuto;

    public AziendaNoleggio(){
        this.parcoAuto = new HashSet<>();
    }

    public boolean aggiungiAuto(Auto auto){
        if(auto != null){
            if(this.parcoAuto.add(auto) == true){
                System.out.println("Auto inserita correttamente");
                return true;
            } else if (this.parcoAuto.add(auto) == false) {
                System.out.println("Auto già presente");
                return false;
            }
        }
        System.out.println("Errore!");
        return false;
    }

    public boolean rimuoviAuto(String numeroTarga){
        for(Auto auto : this.parcoAuto){
            if(auto.getTarga().equals(numeroTarga)){
                System.out.println("Rimozione affettuata con successo\n");
                return this.parcoAuto.remove(auto);
            }
        }
        System.out.println("Numero di targa non presente nel parco auto\n");
        return false;
    }

    public Auto cercaAuto(String numeroTarga){
        Iterator<Auto> iterator = this.parcoAuto.iterator();
        while ((iterator.hasNext())){
            Auto auto = iterator.next();
            if(auto.getTarga().equals(numeroTarga)){
                return auto;
            }
        }
        System.out.println("Numero di targa non presente nel parco auto");
        return null;
    }

    public void visualizzaAutoDisponibili(){
        System.out.println("Parco auto disponibili\n------------------------");
        Iterator<Auto> iterator = this.parcoAuto.iterator();
        while ((iterator.hasNext())) {
            Auto auto = iterator.next();
            auto.infoAuto();
        }
        System.out.println("------------------------");
    }

    public double calcolaPrezzoNoleggio(String numeroTarga, int giorni){
        double totale = 0;

        Auto auto = cercaAuto(numeroTarga);
        totale = auto.getPrezzo() * giorni;

        return  totale;
    }
}
