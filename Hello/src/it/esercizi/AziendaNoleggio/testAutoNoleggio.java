package it.esercizi.AziendaNoleggio;

import java.util.Scanner;

public class testAutoNoleggio {
    public static void main(String[] args){
        Scanner in = new Scanner((System.in));
        String targa;
        AziendaNoleggio piuccioRent = new AziendaNoleggio();
        Auto mercedes = new Auto("KR643TZ", "Mercedes-Benz", "CLK", 34);
        Auto fiat500 = new Auto("VB124WE", "FIAT", "500", 12);
        Auto bmw = new Auto("GB824TR", "BMW", "Serie 4", 76.50);

        piuccioRent.aggiungiAuto(mercedes);
        piuccioRent.aggiungiAuto(fiat500);
        piuccioRent.aggiungiAuto(bmw);

        piuccioRent.rimuoviAuto("GB824TR");
        piuccioRent.cercaAuto("KR643TZ");
        piuccioRent.visualizzaAutoDisponibili();

        System.out.println(("Inserisci la targa dell'auto che hai scelto di noleggiare tra quelle disponibili:"));
        targa = in.nextLine();
        System.out.println("Inserisci il numero di giorni desiderati per il noleggio");
        int giorni = in.nextInt();
        System.out.println("Il preventivo del noleggio di "+giorni+
                " giorni per il veicolo selezionato ammonta a: "+
                String.format("%.2f", piuccioRent.calcolaPrezzoNoleggio(targa, giorni))+"€");


    }
}
