package it.esercizi;

import java.util.Scanner;

public class es2 {
    public  static void main(String[] args){
        Scanner in = new Scanner(System.in);
        char carattere;
        String frase;
        System.out.println("Inserisci il testo");
        frase = in.nextLine();
        System.out.println("Inserisci il carattere da ricercare");
        carattere = in.nextLine().charAt(0);

        boolean result = cerca(frase, carattere);
        int occorrenze = contaOccorrenze(frase, carattere);
        int trovato = primaOccorrenza(frase,carattere);
        System.out.println("Il risultato della ricerca è: "+result);
        System.out.println("Sono stati trovati "+occorrenze+" occorrenze");
        System.out.println("La prima occorrenza è stata trovata all'indice "+trovato);



    }

    public static boolean cerca(String frase, char carattere){
        for(int i = 0; i < frase.length(); i++){
            if(frase.charAt(i) == carattere) {
                return true;
            }
        }
        return false;
    }

    public static int contaOccorrenze(String frase, char carattere){
        int conta = 0;
        for(int i = 0; i < frase.length(); i++){
            if(frase.charAt(i) == carattere) {
                conta++;
            }
        }
        return conta;
    }

    public static int primaOccorrenza(String frase, char carattere){
        int found = 0;
        for(int i = 0; i < frase.length(); i++){
            if(frase.charAt(i) == carattere) {
                return found = i;
            }
        }
        return found;
    }
}
