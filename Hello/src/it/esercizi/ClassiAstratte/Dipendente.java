package it.esercizi.ClassiAstratte;

public abstract class Dipendente {
    protected String nome;
    protected int codice;
    public Dipendente(String nome, int codice){
        this.nome = nome;
        this.codice = codice;
    }

    public abstract double calcolaStipendio();
}
