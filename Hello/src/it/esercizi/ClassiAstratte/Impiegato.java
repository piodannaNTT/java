package it.esercizi.ClassiAstratte;

public class Impiegato extends Dipendente {
    private double stipendioBase;

    public Impiegato(String nome, int codice, double stipendioBase) {
        super(nome, codice);
        this.stipendioBase = stipendioBase;
    }

    @Override
    public double calcolaStipendio() {
        return this.stipendioBase+((this.stipendioBase*10)/100);
    }

    public void infoImpiegato(){
        System.out.println("Nome impiegato: "+this.nome+
                "\nCodice impiegato: "+this.codice+
                "\nStipendo con bonus del 10%: "+calcolaStipendio()
                    );
    }
}
