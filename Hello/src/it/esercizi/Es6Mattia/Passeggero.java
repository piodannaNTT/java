package it.esercizi.Es6Mattia;

public class Passeggero {
    private String nome;
    private String nazionalita;
    private String sigla;
    private String postoAssegnato;
    private String tipologiaPasto;
    public Passeggero(){

    }
    public Passeggero(String nome, String nazionalita, String sigla, String postoAssegnato, String tipologiaPasto){
        this.nome = nome;
        this.nazionalita = nazionalita;
        this.sigla = sigla;
        this.postoAssegnato = postoAssegnato;
        this.tipologiaPasto = tipologiaPasto;
    }

    public String getNome() {
        return nome;
    }

    public String getNazionalita() {
        return nazionalita;
    }

    public String getSigla() {
        return sigla;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setNazionalita(String nazionalita) {
        this.nazionalita = nazionalita;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
    public String getTipologiaPasto() {
        return tipologiaPasto;
    }

    public String getPostoAssegnato() {
        return postoAssegnato;
    }
    public void setPostoAssegnato(String postoAssegnato) {
        this.postoAssegnato = postoAssegnato;
    }
    public void setTipologiaPasto(String tipologiaPasto) {
        this.tipologiaPasto = tipologiaPasto;
    }

    public String toString(){
        return "Nome: "+this.nome+"\nNazionalità: "+this.nazionalita+"\nSigla volo: "+this.sigla+"\n";
    }
}
