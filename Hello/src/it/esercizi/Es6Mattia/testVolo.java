package it.esercizi.Es6Mattia;

public class testVolo {
    public static void main(String[] args){
        Aereoporto roma = new Aereoporto("Fiumicino", "Roma", "RM");
        Aereoporto milano = new Aereoporto("Malpensa", "Milano", "MI");
        Aereoporto toronto = new Aereoporto("Pearson", "Toronto", "TOR");
        Aereoporto newYork = new Aereoporto("J.F.Kennedy", "New York", "NYK");
        Passeggero p1 = new Passeggero("Pio D'Anna", "Italiana", "AZ108", "16F", "carnivoro");
        Passeggero p2 = new Passeggero("Mattia Consonni", "Italiana", "AZ108", "2B", "vegetariano");
        VoloNonDiretto voloNonDiretto = new VoloNonDiretto("BA202", roma, newYork, "AY356", 160, 2);

        voloNonDiretto.addScalo(milano);
        voloNonDiretto.addScalo(toronto);

        voloNonDiretto.addPasseggero(p1);
        voloNonDiretto.addPasseggero(p2);

        voloNonDiretto.infoVolo();

    }
}
