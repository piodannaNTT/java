package it.esercizi.Es6Mattia;
import java.util.Scanner;

public class Volo {
    protected String sigla;
    protected Aereoporto partenza;
    protected Aereoporto destinazione;
    protected String aeromobile;
    protected Passeggero[] passeggeri;
    protected int riempimento;

    public Volo(String sigla, Aereoporto partenza, Aereoporto destinazione, String aeromobile, int maxPosti){
        this.sigla = sigla;
        this.partenza = partenza;
        this.destinazione = destinazione;
        this.aeromobile = aeromobile;
        while (maxPosti <= 0){
            System.out.println("ERRORE! Inserire un numero massimo di passeggeri maggiore di 0");
            maxPosti = (new Scanner(System.in)).nextInt();
        }
        this.passeggeri = new Passeggero[maxPosti];
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public Aereoporto getPartenza() {
        return partenza;
    }

    public void setPartenza(Aereoporto partenza) {
        this.partenza = partenza;
    }

    public Aereoporto getDestinazione() {
        return destinazione;
    }

    public void setDestinazione(Aereoporto destinazione) {
        this.destinazione = destinazione;
    }

    public String getAeromobile() {
        return aeromobile;
    }

    public void setAeromobile(String aeromobile) {
        this.aeromobile = aeromobile;
    }

    public Passeggero[] getPasseggeri() {
        return passeggeri;
    }

    public boolean addPasseggero(Passeggero passeggero){
        if(this.riempimento==0){
            this.passeggeri[0] = passeggero;
            this.riempimento++;
            return true;
        }else{
            for(int i = 0; i<this.riempimento; i++){
                if( passeggero.getPostoAssegnato().equals(this.passeggeri[i].getPostoAssegnato()) ) {
                    System.out.println("Posto già assegnato");
                    return false;
                }
            }
        }
        this.passeggeri[this.riempimento] = passeggero;
        this.riempimento++;
        return true;
        /*
        for(int i = 0; i<this.riempimento; i++){
            if(this.passeggeri[i] != null && passeggero.getPostoAssegnato().equals(this.passeggeri[i].getPostoAssegnato())) {
                System.out.println("Posto già assegnato");
                return false;
            }
        }
        for(i = 0; i<this.passeggeri.length; i++){
            if(this.passeggeri[i] == null){
                this.passeggeri[i] = passeggero;
                return true;
            }
        }
        return false;

         */
    }

    public void infoVolo(){
        System.out.println("Sigla volo: "+this.sigla+
                "\nAereoporto di partenza: "+this.partenza.getCittà()+" "+this.partenza.getNome()+
                "\nAereoporto di destinazione: "+this.destinazione.getCittà()+" "+this.destinazione.getNome());
    }

    public String[] getNomiPasseggeri(){
        String[] nomi = new String[this.passeggeri.length];
        for(int i = 0; i< this.passeggeri.length; i++){
            nomi[i] = this.passeggeri[i].getNome();
        }
        return nomi;
    }

    public Passeggero[] elencoPostiPassegeriVegetariani(){
        int count = 0;
        for(int i = 0; i< this.passeggeri.length; i++){
            if(this.passeggeri[i] != null && this.passeggeri[i].getTipologiaPasto().equals("vegetariano"))
                count++;
        }
        Passeggero[] vegetariani = new Passeggero[count];
        for(int i = 0, j = 0; i< this.passeggeri.length; i++){
            if(this.passeggeri[i] != null && this.passeggeri[i].getTipologiaPasto().equals("vegetariano")){
                vegetariani[j] = this.passeggeri[i];
                j++;
            }
        }
        return vegetariani;
    }

}
