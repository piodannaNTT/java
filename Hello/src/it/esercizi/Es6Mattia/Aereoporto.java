package it.esercizi.Es6Mattia;

import java.util.Stack;

public class Aereoporto {
    private String nome;
    private String città;
    private String sigla;

    public Aereoporto(String nome, String città, String sigla){
        this.nome = nome;
        this.città = città;
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public String getCittà() {
        return città;
    }

    public String getSigla() {
        return sigla;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCittà(String città) {
        this.città = città;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
