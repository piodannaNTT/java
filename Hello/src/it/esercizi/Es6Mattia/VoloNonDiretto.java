package it.esercizi.Es6Mattia;

public class VoloNonDiretto extends Volo{
    private Aereoporto[] scali;
    private int riempimento = 0;
    public VoloNonDiretto(String sigla, Aereoporto partenza, Aereoporto destinazione, String aeromobile, int maxPosti, int numeroScali) {
        super(sigla, partenza, destinazione, aeromobile, maxPosti);
        this.scali = new Aereoporto[numeroScali];
    }

    public Aereoporto[] getScali() {
        return scali;
    }

    public void setScali(Aereoporto[] scali) {
        this.scali = scali;
    }

    public void addScalo(Aereoporto aereoporto){
        if(this.riempimento < this.scali.length){
            this.scali[this.riempimento] = aereoporto;
            this.riempimento++;
        }
    }

    @Override
    public void infoVolo() {
        System.out.println("\nSigla volo: "+this.sigla+
                "\nAereoporto di partenza: "+this.partenza.getCittà()+" - "+this.partenza.getNome()+"\n------------------");
        for(int i = 0; i<this.riempimento; i++){
            System.out.println("Scalo n°"+(i+1)+" - Aereoporto intermedio: "+this.scali[i].getCittà()+" - "+this.scali[i].getNome());
        }
        System.out.println("------------------\nAereoporto di destinazione: "+this.destinazione.getCittà()+" - "+this.destinazione.getNome());
    }
}
