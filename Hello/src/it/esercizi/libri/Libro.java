package it.esercizi.libri;

import java.util.ArrayList;
import java.util.Date;

public class Libro {
    private String titolo;
    private String ISBN;
    private String autore;
    private Date anno_di_uscita;

    public Libro(){
    }
    public Libro(String titolo, String ISBN, String autore, Date anno_di_uscita ){
        this.titolo = titolo;
        this.ISBN = ISBN;
        this.autore = autore;
        this.anno_di_uscita = anno_di_uscita;
    }

    public String getTitolo(){
        return this.titolo;
    }

    public String getISBN(){
        return this.ISBN;
    }

    public String getAutore(){
        return this.autore;
    }

    public Date getAnno_di_uscita(){
        return this.anno_di_uscita;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public void setAutore(String autore) {
        this.autore = autore;
    }

    public void setAnno_di_uscita(Date anno_di_uscita) {
        this.anno_di_uscita = anno_di_uscita;
    }

    public void stampaInfo(){
        System.out.println("Titolo: "+this.titolo+
                            "Codice ISBN: "+this.ISBN+
                            "Autore : "+this.autore+
                            "Anno d'uscita: "+this.anno_di_uscita
        );
    }

    public ArrayList<String> ottieniInfo(){
        ArrayList<String> dati = new ArrayList<String>();
        dati.add(this.titolo);
        dati.add(this.ISBN);
        dati.add(this.autore);
        dati.add(this.anno_di_uscita.toString());
        return dati;
    }

}
