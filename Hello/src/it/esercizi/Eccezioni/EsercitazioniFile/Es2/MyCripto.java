package it.esercizi.Eccezioni.EsercitazioniFile.Es2;

import it.esercizi.Eccezioni.EsercitazioniFile.Es1.MyFile;

import java.io.*;

public class MyCripto {
    public static void main(String[] args) {
        String inPath = System.getProperty("user.home") + "\\OneDrive - NTT DATA EMEAL\\Desktop\\inputInChiaro.txt";
        File inFile = new File(inPath);
        String outPath = System.getProperty("user.home") + "\\OneDrive - NTT DATA EMEAL\\Desktop\\outputCriptato.txt";
        File outFile = new File(outPath);
        FileWriter writer = null;
        BufferedReader reader = null;
        String line = "";

        try {
            if(MyFile.creaFile(inFile)){
                writer = new FileWriter(inPath);
                writer.write("Oggi è proprio una bella giornata per criptare");
                writer.close();

                if(MyFile.creaFile(outFile)){
                    reader = new BufferedReader(new FileReader(inFile.getPath()));
                    writer = new FileWriter(outFile.getPath());
                    while((line = reader.readLine()) !=  null){
                        writer.write(crittografiaCesare(line));
                    }
                    writer.close();
                    reader.close();
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String crittografiaCesare(String linea){
        String result = "";
        char carattere;
        for (int i = 0; i < linea.length(); i++){
            carattere = (char) (linea.charAt(i) + 3);
            result = result + carattere;
        }
        return result;
    }
}
