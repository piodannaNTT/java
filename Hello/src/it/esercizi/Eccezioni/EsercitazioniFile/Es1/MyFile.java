package it.esercizi.Eccezioni.EsercitazioniFile.Es1;

import java.io.*;

public class MyFile {
    public static void main(String[] args) {
        String path = System.getProperty("user.home") + "\\OneDrive - NTT DATA EMEAL\\Desktop\\input.txt";
        File file = new File(path);
        FileWriter writer = null;

        if(creaFile(file)){
            try {
                writer = new FileWriter(path);
                writer.write("1,2,3,4,5\n");
                writer.write("6,7,8,9,10\n");
                writer.write("11,12,13,14,15\n");
                writer.close();

                int risultato = leggiNumeri(file);

                String pathOut = System.getProperty("user.home") + "\\OneDrive - NTT DATA EMEAL\\Desktop\\output.txt";
                File outFile = new File(pathOut);

                if(creaFile(outFile)){
                    writer = new FileWriter(pathOut);
                    writer.write(String.valueOf(risultato));
                    writer.close();
                }

            }catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static boolean creaFile(File file){
        try {
            if(!file.createNewFile()){
                System.out.println("Errore nella creazione del file");
                return false;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println("File creato correttamente");
        return true;
    }

    public static int leggiNumeri(File file){
        int somma = 0;

        try{
            BufferedReader reader = new BufferedReader(new FileReader(file.getPath()));
            String line;
            char car;
            String temp = "";
            while((line = reader.readLine()) !=  null){
                for(int i = 0; i < line.length(); i++){
                    car = line.charAt(i);
                    if(Character.isDigit(car)){
                        temp = temp+String.valueOf(car);
                    }
                    if(car == ',' || i==line.length()-1){
                        somma += Integer.parseInt(temp);
                        temp = "";
                    }
                }
            }
            System.out.println(somma);
            return somma;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}

