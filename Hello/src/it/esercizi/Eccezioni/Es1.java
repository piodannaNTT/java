package it.esercizi.Eccezioni;

import java.util.Scanner;

public class Es1 {
    public static void main(String[] args){
        Scanner in  = new Scanner(System.in);
        int input;
        System.out.println("Inserisci il numero che vuoi dividere");
        input = in.nextInt();
        int risultato = divisioneCasuale(input);
        if(risultato!= 0)
            System.out.println("Risultato della divisione: "+risultato);

    }

    public static int divisioneCasuale(int number){
        int risultato = 0;
        int casuale = (int)(Math.random() * 3);
        try {
            System.out.println("numero causale: "+casuale);
            risultato = number/casuale;
            return risultato;
        }catch (ArithmeticException exception){
            System.out.println("Divisione per 0, operazione non consentita");
        }
        return risultato;
    }
}
