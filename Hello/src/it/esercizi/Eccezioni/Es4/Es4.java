package it.esercizi.Eccezioni.Es4;

import java.util.Scanner;

public class Es4 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        String s1, s2;
        int posizione;
        System.out.println("Inserisci la stringa 1: ");
        s1 = in.nextLine();
        System.out.println("Inserisci la stringa 2: ");
        s2 = in.nextLine();
        System.out.println("Inserisci la posizione: ");
        posizione = in.nextInt();

        try {
            stringheSimili(s1, s2, posizione);
        } catch (StringheDiverseException e) {
            System.out.println(e.getMessage());
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }catch (StringheVuoteException e){
            System.out.println(e.getMessage());
        }

    }

    public static void stringheSimili(String a, String b, int p) throws StringheDiverseException, StringheVuoteException {
        if (p < 0){
            throw new IllegalArgumentException("La posizione inserita è un numero minore di 0");
        }else if(a.equals("") || b.equals("")){
            throw new StringheVuoteException("Una o entrambe le stringhe sono vuote");
        }else if( !(a.substring(0, p).equals(b.substring(0, p))) ){
            throw new StringheDiverseException("La due sottostringhe sono diverse");
        }
        if((a.substring(0, p).equals(b.substring(0, p)))){
            System.out.println("Le due stringhe sono uguali");
        }
    }
}
