package it.esercizi.Eccezioni.Es4;

public class StringheVuoteException extends Exception{
    public StringheVuoteException(){
        super();
    }
    public StringheVuoteException(String message){
        super(message);

    }
}
