package it.esercizi.Eccezioni.Es4;

public class StringheDiverseException extends Exception{
    public StringheDiverseException(){
        super();
    }
    public StringheDiverseException(String message){
        super(message);

    }
}
