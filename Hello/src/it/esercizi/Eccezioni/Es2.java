package it.esercizi.Eccezioni;

import java.util.Scanner;

public class Es2 {
    public static void main(String[] args){
        int array[] = {1, 2, 3, 4, 5, 6};
        int posizione = 7;
        inserimentoOltreLimiteNoTryCatch(array, posizione);

    }
    public static void inserimentoOltreLimite(int[] array, int posizione){
        Scanner in = new Scanner(System.in);
        int numero = 0;
        try{
            System.out.println("Inserisci il numero");
            numero = in.nextInt();
            array[posizione] = numero;
            System.out.println("Numero "+numero+" inserito correttamente alla posizione "+posizione);
            System.out.println("[ ");
            for(int i = 0; i < array.length; i++)
                System.out.print(array[i]+" ");
            System.out.println("]");
        }catch (IndexOutOfBoundsException exception){
            System.out.println("Errore, si è tentato di inserire un numero fuori dai limiti dell'array");
        }finally {
            in.close();
        }
    }

    public static void inserimentoOltreLimiteNoTryCatch(int[] array, int posizione){

        Scanner in = new Scanner(System.in);
        int numero = 0;
        System.out.println("Inserisci il numero");
        numero = in.nextInt();
        if(posizione >= array.length){
            System.out.println("Errore, si è tentato di inserire un numero fuori dai limiti dell'array");
        }else {
            array[posizione] = numero;
            System.out.println("Numero "+numero+" inserito correttamente alla posizione "+posizione);
            System.out.print("[ ");
            for(int i = 0; i < array.length; i++)
                System.out.print(array[i]+" ");
            System.out.println("]");
            in.close();
        }
    }
}
