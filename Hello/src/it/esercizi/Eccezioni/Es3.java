package it.esercizi.Eccezioni;

import java.util.Scanner;

public class Es3 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Inserisci un numero");
        String numero = in.nextLine();
        checkFormatoOfImaginariNumber(numero);
    }
    /*

    public static void checkFormatoOfImaginariNumber(String number){
        if(number.startsWith("i")){
            if(controlla(number.substring(1))){
                System.out.println(number+" --> numero immaginario");
            }
        }else if(controlla(number)){
            System.out.println(number+" --> numero reale");
        }else {
            System.out.println(number+" --> non è un numero");
        }
    }

     */

    public static boolean checkFormatoOfImaginariNumber(String number){
        double conversione = 0;
        try{
            if(number.startsWith("i")){
                conversione = Double.parseDouble(number.substring(1));
                System.out.println(number+" --> numero immaginario");
                return true;
            }
            conversione = Double.parseDouble(number);
            System.out.println(number+" --> numero reale");
            return true;

        }catch (NumberFormatException exception){
            System.out.println(number+" --> non è un numero");
        }
        return false;
    }

    public static boolean controlla(String string){
        for(int i = 0; i < string.length(); i++){
            char carattere = string.charAt(i);
            if(!Character.isDigit(carattere))
                return false;
        }
        return true;
    }
}
