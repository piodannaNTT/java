package it.esercizi.NegozioAbbigliamento;

import java.util.HashMap;
import java.util.Map;

public class Ordine {
    private Cliente cliente;
    private HashMap<Prodotto, Integer> prodottiAcquistarti;

    public Ordine(Cliente cliente){
        this.cliente = cliente;
        this.prodottiAcquistarti = new HashMap<>();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public HashMap<Prodotto, Integer> getProdottiAcquistarti() {
        return prodottiAcquistarti;
    }

    public void setProdottiAcquistarti(HashMap<Prodotto, Integer> prodottiAcquistarti) {
        this.prodottiAcquistarti = prodottiAcquistarti;
    }

    public void infoOrdine(){
        System.out.println("Dettagli ordine\n----------------");
        System.out.println("Codice cliente: "+this.cliente.getCodiceCliente());
        for(Map.Entry<Prodotto, Integer> entry : this.prodottiAcquistarti.entrySet()){
            entry.getKey().infoProdotto();
            System.out.println("Quantità acquistata: "+entry.getValue()+"\n");
        }
    }

    public boolean aggiungiOrdine(Prodotto prodotto, int quantitaAcquistata){
        if(prodotto != null && quantitaAcquistata > 0){
            this.prodottiAcquistarti.put(prodotto, quantitaAcquistata);
            return true;
        }
        return false;
    }
}
