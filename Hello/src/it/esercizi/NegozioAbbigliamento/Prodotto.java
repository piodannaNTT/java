package it.esercizi.NegozioAbbigliamento;

public class Prodotto {
    public enum TipoProdotto{
        MAGLIA,
        PANTALONI,
        SCARPE
    }
    private int codiceProdotto;
    private TipoProdotto tipo;
    private String descrizione;
    private double prezzo;
    private int quantitaDisponibile;

    public Prodotto(int codiceProdotto, TipoProdotto tipo, String descrizione, double prezzo, int quantitaDisponibile) {
        this.codiceProdotto = codiceProdotto;
        this.tipo = tipo;
        this.descrizione = descrizione;
        this.prezzo = prezzo;
        this.quantitaDisponibile = quantitaDisponibile;
    }

    public int getCodiceProdotto() {
        return codiceProdotto;
    }

    public void setCodiceProdotto(int codiceProdotto) {
        this.codiceProdotto = codiceProdotto;
    }

    public TipoProdotto getTipo() {
        return tipo;
    }

    public void setTipo(TipoProdotto tipo) {
        this.tipo = tipo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public int getQuantitàDisponibile() {
        return quantitaDisponibile;
    }

    public void setQuantitàDisponibile(int quantitàDisponibile) {
        this.quantitaDisponibile = quantitàDisponibile;
    }

    public void infoProdotto(){
        System.out.println("Codice prodotto: "+this.codiceProdotto+"\nTipologia: "+this.tipo+
                "\nDescrizione prodotto: "+this.descrizione+"\nPrezzo: "+this.prezzo+
                "\nQuantità disponibile: "+this.quantitaDisponibile);
    }
}
