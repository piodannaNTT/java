package it.esercizi.NegozioAbbigliamento;

import java.lang.ref.Cleaner;
import java.util.*;

public class NegozioAbbigliamento {
    private HashMap<Integer, Prodotto> prodottiDisponibili;
    private HashMap<Integer, Cliente> clienti;
    HashSet<Ordine> ordini;

    public NegozioAbbigliamento(){
        this.prodottiDisponibili = new HashMap<>();
        this.clienti = new HashMap<>();
        this.ordini = new HashSet<>();
    }

    public boolean aggiungiProdotto(Prodotto prodotto){
        if(prodotto != null){
            this.prodottiDisponibili.put(prodotto.getCodiceProdotto(), prodotto);
            return true;
        }
        System.out.println("Inserimento non riuscito");
        return false;
    }

    public boolean rimuoviProdotto(int codiceProdotto){
        if(this.prodottiDisponibili.containsKey(codiceProdotto)){
            this.prodottiDisponibili.remove(codiceProdotto);
            return true;
        }
        System.out.println("Prodotto non presente tra quelle dispobili per la rimozione");
        return false;
    }

    public boolean aggiungiCliente(Cliente cliente){
        if(cliente != null){
            this.clienti.put(cliente.getCodiceCliente(), cliente);
            return true;
        }
        System.out.println("Inserimento non riuscito");
        return false;
    }

    public boolean rimuoviCliente(int codiceCliente){
        if(this.clienti.containsKey(codiceCliente)){
            this.clienti.remove(codiceCliente);
            return true;
        }
        System.out.println("Cliente non presente tra quelli dispobili per la rimozione");
        return false;
    }

    public boolean effettuaOrdine(Ordine ordine){
        if(ordine != null){
            this.ordini.add(ordine);
            return true;
        }
        System.out.println("Ordine fallito");
        return false;
    }

    public void visualizzaProdottiDisponibili(){
        System.out.println("Prodotti disponibili\n------------------");
        for(Map.Entry<Integer, Prodotto> entry : this.prodottiDisponibili.entrySet()){
            entry.getValue().infoProdotto();
            System.out.println("------------------");
        }
    }

    public void visualizzaClienti(){
        System.out.println("Clienti\n------------------");
        for(Map.Entry<Integer, Cliente> entry : this.clienti.entrySet()){
            entry.getValue().infoCliente();
        }
    }

    public void visualizzaOrdini(){
        System.out.println("\nTotale ordini\n----------------");
        Iterator<Ordine> iterator = this.ordini.iterator();
        while(iterator.hasNext()){
            Ordine ordine = iterator.next();
            ordine.infoOrdine();
        }
    }
}
