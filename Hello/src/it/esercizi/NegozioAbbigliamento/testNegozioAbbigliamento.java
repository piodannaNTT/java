package it.esercizi.NegozioAbbigliamento;

public class testNegozioAbbigliamento {
    public static void main(String[] args){
        NegozioAbbigliamento negozioAbbigliamento = new NegozioAbbigliamento();

        Prodotto jeans = new Prodotto(352, Prodotto.TipoProdotto.PANTALONI, "Jean accattivante", 59.90, 25);
        Prodotto nike = new Prodotto(533, Prodotto.TipoProdotto.SCARPE, "Nike AirForce 1", 119.90, 15);
        Prodotto t_shirt = new Prodotto(123, Prodotto.TipoProdotto.MAGLIA, "T-Shirt Adidas", 28.90, 30);

        Cliente pio = new Cliente(6324, "Pio D'Anna", 25);
        Cliente mattia = new Cliente(1524, "Mattia Consonni", 24);
        Cliente walter = new Cliente(3285, "Walter Garcia", 23);

        Ordine ordine1= new Ordine(pio);
        ordine1.aggiungiOrdine(jeans, 2);
        negozioAbbigliamento.effettuaOrdine(ordine1);

        Ordine ordine2 = new Ordine(walter);
        ordine2.aggiungiOrdine(jeans, 3);
        negozioAbbigliamento.effettuaOrdine(ordine2);

        Ordine ordine3= new Ordine(pio);
        ordine3.aggiungiOrdine(jeans, 3);
        negozioAbbigliamento.effettuaOrdine(ordine3);


        negozioAbbigliamento.aggiungiProdotto(jeans);
        negozioAbbigliamento.aggiungiProdotto(nike);
        negozioAbbigliamento.aggiungiProdotto(t_shirt);

        negozioAbbigliamento.aggiungiCliente(pio);
        negozioAbbigliamento.aggiungiCliente(mattia);
        negozioAbbigliamento.aggiungiCliente(walter);

        //negozioAbbigliamento.visualizzaOrdini();

        //negozioAbbigliamento.visualizzaProdottiDisponibili();
        //negozioAbbigliamento.visualizzaClienti();
        negozioAbbigliamento.rimuoviProdotto(352);
        negozioAbbigliamento.visualizzaProdottiDisponibili();



    }
}
