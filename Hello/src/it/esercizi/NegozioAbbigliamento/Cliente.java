package it.esercizi.NegozioAbbigliamento;

public class Cliente {
    private int codiceCliente;
    private String nome;
    private int eta;

    public Cliente(int codiceCliente, String nome, int eta) {
        this.codiceCliente = codiceCliente;
        this.nome = nome;
        this.eta = eta;
    }

    public int getCodiceCliente() {
        return codiceCliente;
    }

    public void setCodiceCliente(int codiceCliente) {
        this.codiceCliente = codiceCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public void infoCliente(){
        System.out.println("Codice cliente: "+this.codiceCliente+"\nNome: "+this.nome+"\nEtà: "+this.eta+"\n");
    }
}
