package it.esercizi.LibroMattia;

import java.util.Scanner;

public class testBiblioteca {
    public static void main(String[] args){
        String titolo;
        Scanner in = new Scanner(System.in);

        Biblioteca biblioteca = new Biblioteca();
        LibroStampato libroStampato1 = new LibroStampato("Interstellar", "Jack Ryan", "20/09/1990", 540);
        LibroStampato libroStampato2 = new LibroStampato("Non mi piace leggere", "Piuccio", "05/04/2024", 33);
        LibroDigitale libroDigitale1 = new LibroDigitale("Harry Potter", "J.K.Rowling", "12/05/1995", "MOBI");
        biblioteca.addLibro(libroStampato1);
        biblioteca.addLibro(libroStampato2);
        biblioteca.addLibro(libroDigitale1);

        //Test numero di pagine totali
        System.out.println("Pagine totali libro stampati: "+biblioteca.calcNumPagTotaliLibriStampati());
        //Test ricerca libro tramite titolo
        System.out.println("Inserisci il titolo del libro da ricercare: ");
        titolo = in.nextLine();
        Libro libro = biblioteca.searchLibro(titolo);
        //Visualizzazzione dell'intera biblioteca
        biblioteca.visualizzaLibri();

    }
}
