package it.esercizi.LibroMattia;

import java.util.Date;

public class LibroDigitale extends Libro{
    private String formatoDigitale;
    public LibroDigitale(String titolo, String autore, String data_pubblicazione, String formatoDigitale) {
        super(titolo, autore, data_pubblicazione);
        this.formatoDigitale = formatoDigitale;
    }

    public String getFormatoDigitale() {
        return formatoDigitale;
    }

    public void setFormatoDigitale(String formatoDigitale) {
        this.formatoDigitale = formatoDigitale;
    }
}
