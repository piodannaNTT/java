package it.esercizi.LibroMattia;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Libro {
    protected String titolo;
    protected String autore;
    protected LocalDate data_pubblicazione;

    public Libro(String titolo, String autore, String data_pubblicazione){
        this.titolo = titolo;
        this.autore = autore;
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        this.data_pubblicazione = LocalDate.parse(data_pubblicazione,DateTimeFormatter.ofPattern("dd/MM/yyyy") );
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getAutore() {
        return autore;
    }

    public void setAutore(String autore) {
        this.autore = autore;
    }

    public LocalDate getData_pubblicazione() {
        return data_pubblicazione;
    }

    public void setData_pubblicazione(LocalDate data_pubblicazione) {
        this.data_pubblicazione = data_pubblicazione;
    }

    public void infoLibro(){
        System.out.println("Titolo: "+this.titolo+"\nAutore: "+this.autore+
                "\nData di pubblicazione: "+this.data_pubblicazione);
    }
}
