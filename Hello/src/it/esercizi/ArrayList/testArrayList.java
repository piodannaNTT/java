package it.esercizi.ArrayList;

import java.util.ArrayList;
import java.util.Scanner;

public class testArrayList {
    public static void main(String[] args){
        ArrayList<Studente> studenti = new ArrayList<>();
        Scanner in = new Scanner(System.in);
        String condition;
        String temp;
        do{
            /*
            System.out.println("Inserisci il nome dello studente: ");
             temp = in.nextLine();
            System.out.println("Inserisci il nome dello studente: ");
            int voto = in.nextInt();

            Studente studente = new Studente(nome, voto);
            studenti.add(studente);


             */
            System.out.println("Invio per un nuovo studente - exit per uscire");
            condition = in.nextLine();
        }while (!condition.equals("exit"));

        System.out.println("La media dei voti di tutti gli studenti inseriti è: "+calcolaMedia(studenti));
    }

    public static int calcolaMedia(ArrayList<Studente> studenti){
        int somma = 0;
        for(Studente studente : studenti){
            somma+=studente.getVoto();
        }
        return somma/studenti.size();
    }

    public static void clear(ArrayList<Studente> studenti){
        for(Studente studente : studenti){
            studenti.remove(studente);
        }
    }
}
