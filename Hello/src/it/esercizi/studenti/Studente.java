package it.esercizi.studenti;

import java.util.HashMap;
import java.util.Map;

public class Studente {
    /**
     * Variabili d'istanza dello studente, abbiamo il nome ed il cognome che sono due stringhe
     * ed abbiamo anche la matricola che è un intero e un HashMap che viene utilizzata per
     * immagazzinare gli esami ed i voti corrispondenti
     *
     */
    private String nome;
    private String cognome;
    private int matricola;
    private HashMap<String, Integer> esami;

    /**
     * Costruttore vuoto che inizializza a null le stringhe, a 0 la matricola e crea un
     * HashMap vuota
     * @Studente
     */
    public Studente(){
        this.esami = new HashMap<String, Integer>();
    }

    /**
     * Costruttore che inizializza un nuovo studente e al quale vengono passati i dati quali:
     * nome, cognome, matricola
     * ed inizializza l'HashMap come vuota
     * @Studente
     */
    public Studente(String nome, String cognome, int matricola){
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
        this.esami = new HashMap<String, Integer>();
    }

    /**
     * Restituisce il nome
     * @return String nome
     */
    public String getNome(){
        return this.nome;
    }

    public String getCognome(){
        return this.cognome;
    }

    public int getMatricola(){
        return this.matricola;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public void setCognome(String cognome){
        this.cognome = cognome;
    }

    public void setMatricola(int matricola){
        this.matricola = matricola;
    }

    public HashMap<String, Integer> getEsami(){
        return this.esami;
    }

    /**
     * In questo metodo vado a passare degli esami
     * @param materia String
     * @param voto int
     */
    public void addEsame(String materia, int voto){
        this.esami.put(materia, voto);
    }



    public void tostring(){
        System.out.println("Studente\n" +
                            "Cognome: "+this.cognome+
                            "\nNome: "+this.nome+
                            "\nMatricola: "+this.matricola);
        System.out.println("\nLista degli esami superati");
        for(Map.Entry<String, Integer> esame : esami.entrySet()){
            System.out.println(esame.getKey().toString()+": "+esame.getValue().toString());
        }
    }



}
