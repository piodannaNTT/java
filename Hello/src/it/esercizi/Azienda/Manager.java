package it.esercizi.Azienda;

public class Manager extends Dipendente{
    private String dipartimento;
    public Manager(String nome, double stipendio, String dipartimento) {
        super(nome, stipendio);
        this.dipartimento = dipartimento;
    }

    @Override
    public void stampaInfo() {
        super.stampaInfo();
        System.out.println("Dipartimento di appartenenza: "+this.dipartimento+"\n");
    }
}
