package it.esercizi.Azienda;

public class testAzienda {
    public static void main(String[] args){
        Manager manager = new Manager("Pio D'Anna", 70000, "Amministrazione generale");
        Impiegato impiegato = new Impiegato("Aldo Rosso", 35000, "Front-End");
        manager.stampaInfo();
        impiegato.stampaInfo();
    }
}
