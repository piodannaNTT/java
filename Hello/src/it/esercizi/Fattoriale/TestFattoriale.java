package it.esercizi.Fattoriale;

import java.util.Scanner;

public class TestFattoriale {
    public  static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int num = 0;
        Fattoriale fat = new Fattoriale();
        do{
            System.out.println("Inserisci un numero(-1 per terminare):");
            num = in.nextInt();
            if(num>0){
               fat.setValore(num);
                System.out.println("Il fattoriale di "+fat.getValore()+" è: "+fat.calcolaFattoriale());
            }
        }while(num != -1);
    }
}
