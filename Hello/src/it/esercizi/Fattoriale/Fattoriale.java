package it.esercizi.Fattoriale;

public class Fattoriale {
    int valore;

    public int getValore() {
        return valore;
    }

    public void setValore(int valore) {
        this.valore = valore;
    }

    public int calcolaFattoriale(){
        int fat = 1;
        for(int i = 1; i<=this.valore; i++)
           fat *= i;
        return fat;
    }
}
