package it.esercizi.PrimaProvaDellaMorte;

import java.time.DayOfWeek;
import java.util.ArrayList;

public class ClasseDiAllenamento {
    private String nome;

    private DayOfWeek giornoCorso;
    private String orario;
    private ArrayList<Membro> membri;
    private Istruttore istruttore;
    private  int maxAdesioni;

    /**
     * Costruttore che permettere di inzializzare il membro
     * @param nome
     * @param orario
     * @param istruttore
     * @param maxAdesioni
     */
    public ClasseDiAllenamento(String nome, DayOfWeek giornoCorso, String orario, Istruttore istruttore, int maxAdesioni) {
        this.nome = nome;
        this.giornoCorso = giornoCorso;
        this.orario = orario;
        this.membri = new ArrayList<>();
        this.istruttore = istruttore;
        this.maxAdesioni = maxAdesioni;
    }

    /**
     * Metodo che restituisce il nome del corso
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getOrario() {
        return orario;
    }

    public void setOrario(String orario) {
        this.orario = orario;
    }

    public ArrayList<Membro> getMembri() {
        return membri;
    }

    public void setMembri(ArrayList<Membro> membri) {
        this.membri = membri;
    }

    public Istruttore getIstruttore() {
        return istruttore;
    }
    public DayOfWeek getGiornoCorso() {
        return giornoCorso;
    }

    public int getMaxAdesioni() {
        return maxAdesioni;
    }

    public void setIstruttore(Istruttore istruttore) {
        this.istruttore = istruttore;
    }

    public void addMembro(Membro membro) throws ClassePienaException{
        if(this.membri.size() < this.maxAdesioni){
            for (Membro m : this.membri){
                if(m.getIdMembro().equals(membro.getIdMembro())){
                    System.out.println("Membro già aggiunto al corso");
                    return;
                }
            }
            this.membri.add(membro);
        }else {
            throw new ClassePienaException(membro.getNome()+" non può essere aggiunto alla classe di "+this.getNome()+" in quanto i posti a disposizione sono terminati.");
        }
    }

    public void infoCorso(){
        System.out.println("CLASSE \n-------------\nNome corso:" +this.getNome()+"\nOrario corso: "+this.getOrario()+"\n-------------");
    }
}
