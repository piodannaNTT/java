package it.esercizi.PrimaProvaDellaMorte;

public class AbbonamentoNonValidoException extends Exception{
    public AbbonamentoNonValidoException() {
        super();
    }

    public AbbonamentoNonValidoException(String message) {
        super(message);
    }
}
