package it.esercizi.PrimaProvaDellaMorte;

public class ClassePienaException extends Exception {
    public ClassePienaException() {
        super();
    }

    public ClassePienaException(String message) {
        super(message);
    }
}
