package it.esercizi.PrimaProvaDellaMorte;

import java.time.DayOfWeek;

public class testPalestra {
    public static void main(String[] args){
        GestionePalestra spartanGym = new GestionePalestra();
        //Membri
        Membro pio = new Membro("Pio", "D'Anna", "DNN33P", Membro.Abbonamento.Mensile, "01/04/2024");
        Membro anna = new Membro("Anna", "Crippa", "cs4w4t", Membro.Abbonamento.Annuale, "07/10/2023");
        Membro walter = new Membro("Walter", "Garcia", "dw34sP", Membro.Abbonamento.Giornaliero, "01/04/2024");
        Istruttore simone = new Istruttore("Simone", "De Meis", "f32453", null, null, "Bodybuilding", 10);
        Istruttore mara = new Istruttore("Mara", "Campana", "64f34g", null, null, "Zumba", 3);
        Istruttore paride = new Istruttore("Paride", "Rossi", "hjk242d", null, null, "MMA", 7);
        //Classi di allenamento
        ClasseDiAllenamento calisthenic = new ClasseDiAllenamento("Calisthenics", DayOfWeek.MONDAY, "17:30", simone, 30);
        ClasseDiAllenamento zumba = new ClasseDiAllenamento("Zumba", DayOfWeek.THURSDAY, "20:30", mara, 50);
        ClasseDiAllenamento mma = new ClasseDiAllenamento("MMA", DayOfWeek.MONDAY, "17:30", paride, 2);
        //Aggiunta menbri
        spartanGym.addMembri(pio);
        spartanGym.addMembri(anna);
        spartanGym.addMembri(walter);
        spartanGym.addMembri(simone);
        spartanGym.addMembri(mara);
        spartanGym.addMembri(paride);
        //Aggiunta classe di allenamento
        spartanGym.addClasseDiAllenamento(calisthenic);
        spartanGym.addClasseDiAllenamento(zumba);
        spartanGym.addClasseDiAllenamento(mma);
        //Aggiunta membri ad un corso
        try {
            spartanGym.getCorsoByNome("MMA").addMembro(anna);
            spartanGym.getCorsoByNome("MMA").addMembro(pio);
            spartanGym.getCorsoByNome("Calisthenics").addMembro(walter);

            spartanGym.checkValiditàAbbonamento(pio);
            spartanGym.checkValiditàAbbonamento(walter);
        } catch (ClassePienaException | AbbonamentoNonValidoException e) {
            System.out.println(e.getMessage());
        }

        spartanGym.visualizzaCorsi(DayOfWeek.MONDAY);
    }
}
