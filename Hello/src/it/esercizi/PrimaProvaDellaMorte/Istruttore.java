package it.esercizi.PrimaProvaDellaMorte;

public class Istruttore extends Membro{
    private String specializzazione;
    private int anniDiEsperienza;

    public Istruttore(String nome, String cognome, String idMembro, Abbonamento abbonamento, String dataAbbonamento, String specializzazione, int anniDiEsperienza) {
        super(nome, cognome, idMembro, abbonamento, dataAbbonamento);
        this.specializzazione = specializzazione;
        this.anniDiEsperienza = anniDiEsperienza;
    }


    public String getSpecializzazione() {
        return specializzazione;
    }

    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }

    public int getAnniDiEsperienza() {
        return anniDiEsperienza;
    }

    public void setAnniDiEsperienza(int anniDiEsperienza) {
        this.anniDiEsperienza = anniDiEsperienza;
    }

    @Override
    public void infoMembro() {

        System.out.println("ISTRUTTORE\n-------------\nNome membro: "+this.getNome()+" "+this.getCognome()+
                "\nID membro: "+this.getIdMembro()+"\nSpecializzazione: "+
                this.getSpecializzazione()+"\nAnni di esperienza: "+this.getAnniDiEsperienza()+"\n-------------");
    }
}
