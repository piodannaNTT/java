package it.esercizi.PrimaProvaDellaMorte;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Membro {
    private String nome;
    private String cognome;
    private String idMembro;
    private Abbonamento abbonamento;
    private LocalDate dataAbbonamento;

    public enum Abbonamento{
        Giornaliero,
        Mensile,
        Annuale,
        Istruttore
    }

    public Membro(String nome, String cognome, String idMembro, Abbonamento abbonamento, String dataAbbonamento) {
        this.nome = nome;
        this.cognome = cognome;
        this.idMembro = idMembro;
        this.abbonamento = abbonamento;
        if(dataAbbonamento == null){
            this.dataAbbonamento = null;
        }else{
            this.dataAbbonamento = LocalDate.parse(dataAbbonamento, DateTimeFormatter.ofPattern("dd/MM/yyyy") );
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getIdMembro() {
        return idMembro;
    }

    public void setIdMembro(String idMembro) {
        this.idMembro = idMembro;
    }

    public Abbonamento getAbbonamento() {
        return abbonamento;
    }

    public void setAbbonamento(Abbonamento abbonamento) {
        this.abbonamento = abbonamento;
    }

    public LocalDate getDataAbbonamento(){
        return this.dataAbbonamento;
    }

    public void infoMembro(){
        System.out.println("Nome membro: "+this.getNome()+" "+this.getCognome()+
                "\nID membro: "+this.idMembro+"\nTipo di abbonamento: "+this.getAbbonamento()+"\n-------------");
    }
}
