package it.esercizi.PrimaProvaDellaMorte;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;

public class GestionePalestra {
    private ArrayList<Membro> membri;
    private HashMap<String, Membro.Abbonamento> abbomenti;
    ArrayList<ClasseDiAllenamento> classiDiAllenamento;

    public GestionePalestra(){
        this.membri = new ArrayList<>();
        this.abbomenti = new HashMap<>();
        this.classiDiAllenamento = new ArrayList<>();
    }

    public void addAbbonamento(String id, Membro.Abbonamento abbonamento){
        this.abbomenti.put(id, abbonamento);
    }

    public void addClasseDiAllenamento(ClasseDiAllenamento classeDiAllenamento){
        this.classiDiAllenamento.add(classeDiAllenamento);
    }
    public void addMembri(Membro membro){
        this.membri.add(membro);
        addAbbonamento(membro.getIdMembro(), membro.getAbbonamento());
    }

    public void aggiuntaNuovoClasseDiAllenamento(String nome, DayOfWeek giornoCorso, String orario, Istruttore istruttore, int maxAdesioni){
        ClasseDiAllenamento classeDiAllenamento = new ClasseDiAllenamento(nome, giornoCorso, orario, istruttore, maxAdesioni);
        this.classiDiAllenamento.add(classeDiAllenamento);
    }

    public void checkValiditàAbbonamento(Membro membro) throws AbbonamentoNonValidoException{
        LocalDate data = membro.getDataAbbonamento();
        switch (membro.getAbbonamento()){
            case Giornaliero:
                if (data.isBefore(LocalDate.now())){
                    throw new AbbonamentoNonValidoException("Abbonamento giornaliero scaduto, cacc e sordi straccione");
                } else if (data.isAfter(LocalDate.now())) {
                    System.out.println("Data abbonamento non valida");
                }else if(data.isEqual(LocalDate.now())) {
                    System.out.println("Abbonamento giornaliero valido");
                }
                break;
            case Mensile:
                if(data.plusMonths(1).isBefore(LocalDate.now())){
                    throw new AbbonamentoNonValidoException("Abbonamento mensile scaduto");
                } else if (data.plusMonths(1).isAfter(LocalDate.now())) {
                    System.out.println("Abbonamento mensile valido");
                }
                break;
            case Annuale:
                if(data.plusYears(1).isBefore(LocalDate.now())){
                    throw new AbbonamentoNonValidoException("Abbonamento annuale scaduto");
                } else if (data.plusYears(1).isAfter(LocalDate.now())) {
                    System.out.println("Abbonamento annuale valido");
                }
                break;
            default:
                System.out.println("Casistica non contemplata");
        }
    }
    public ClasseDiAllenamento getCorsoByNome(String nome){
        Iterator<ClasseDiAllenamento> iterator = this.classiDiAllenamento.iterator();
        while (iterator.hasNext()){
            ClasseDiAllenamento classeDiAllenamento = iterator.next();
            if(classeDiAllenamento.getNome().equals(nome)){
                return classeDiAllenamento;
            }
        }
        return null;
    }

    public void visualizzaCorsi(DayOfWeek giornoCorso){
        for (ClasseDiAllenamento classeDiAllenamento : this.classiDiAllenamento){
            if(classeDiAllenamento.getGiornoCorso().equals(giornoCorso)){
                classeDiAllenamento.infoCorso();
                for(Membro membro : classeDiAllenamento.getMembri()){
                    membro.infoMembro();
                }
                classeDiAllenamento.getIstruttore().infoMembro();
                System.out.println("\n");
            }
        }
    }
}

