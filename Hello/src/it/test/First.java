package it.test;

import java.util.Scanner;

public class First {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        String operando1;
        String operando2;
        String operazione;
        String scelta;

        do{
            System.out.println("Inserisci il primo operando");
            operando1 = in.nextLine();
            System.out.println("Inserisci il secondo operando");
            operando2 = in.nextLine();
            System.out.println("Seleziona l'operazione tra le seguenti: \naddizione -> +\nsottrazione -> -\nmoltiplicazione -> *\ndivisione -> /");
            operazione = in.nextLine();

            double op1;
            double op2;
            double risultato;


            switch (operazione){
                case "+" :
                    op1 = Double.parseDouble(operando1);
                    op2 = Double.parseDouble(operando2);
                    risultato = op1 + op2;
                    System.out.println("Il risultato della somma è: "+risultato);
                    break;
                case "-":
                    op1 = Double.parseDouble(operando1);
                    op2 = Double.parseDouble(operando2);
                    risultato = op1 - op2;
                    System.out.println("Il risultato della sottrazione è: "+risultato);
                    break;
                case "*":
                    op1 = Double.parseDouble(operando1);
                    op2 = Double.parseDouble(operando2);
                    risultato = op1 * op2;
                    System.out.println("Il risultato della moltiplicazione è: "+risultato);
                    break;
                case "/":
                    op1 = Double.parseDouble(operando1);
                    op2 = Double.parseDouble(operando2);
                    risultato = op1 / op2;
                    System.out.println("Il risultato della divisione è: "+risultato);
                    break;
                default:
                    System.out.println("Operazione non consentita");
            }


            System.out.println("Scrivere exit per uscire - Invio per continuare ");
            scelta = in.nextLine();

        }while(!scelta.equals("exit"));
        System.out.println("Grazie per averci scelto!!!");
    }
}
