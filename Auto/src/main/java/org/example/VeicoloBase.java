package org.example;
import java.util.ArrayList;

/**
 * Questa classe rappresenta un veicolo di base.
 */
public class VeicoloBase {
    private String tipoVeicolo;
    private int potenzaMotore;
    private ArrayList<Accessorio> accessori;
    private int numeroAccessori;
    private double prezzo;

    /**
     * Costruttore per la classe VeicoloBase.
     *
     * @param tipoVeicolo     Il tipo di veicolo.
     * @param potenzaMotore   La potenza del motore.
     * @param numeroAccessori Il numero massimo di accessori consentiti.
     * @param prezzo          Il prezzo del veicolo.
     */
    public VeicoloBase(String tipoVeicolo, int potenzaMotore, int numeroAccessori, double prezzo) {
        this.tipoVeicolo = tipoVeicolo;
        this.potenzaMotore = potenzaMotore;
        this.accessori = new ArrayList<>();
        this.numeroAccessori = numeroAccessori;
        this.prezzo = prezzo;
    }

    /**
     * Restituisce il tipo di veicolo.
     *
     * @return Il tipo di veicolo.
     */
    public String getTipoVeicolo() {
        return tipoVeicolo;
    }

    /**
     * Imposta il tipo di veicolo.
     *
     * @param tipoVeicolo Il tipo di veicolo da impostare.
     */
    public void setTipoVeicolo(String tipoVeicolo) {
        this.tipoVeicolo = tipoVeicolo;
    }

    /**
     * Restituisce la potenza del motore.
     *
     * @return La potenza del motore.
     */
    public int getPotenzaMotore() {
        return potenzaMotore;
    }

    /**
     * Imposta la potenza del motore.
     *
     * @param potenzaMotore La potenza del motore da impostare.
     */
    public void setPotenzaMotore(int potenzaMotore) {
        this.potenzaMotore = potenzaMotore;
    }

    /**
     * Restituisce la lista di accessori del veicolo.
     *
     * @return La lista di accessori del veicolo.
     */
    public ArrayList<Accessorio> getAccessori() {
        return accessori;
    }

    /**
     * Imposta la lista di accessori del veicolo.
     *
     * @param accessori La lista di accessori da impostare.
     */
    public void setAccessori(ArrayList<Accessorio> accessori) {
        this.accessori = accessori;
    }

    /**
     * Restituisce il numero massimo di accessori consentiti.
     *
     * @return Il numero massimo di accessori consentiti.
     */
    public int getNumeroAccessori() {
        return numeroAccessori;
    }

    /**
     * Imposta il numero massimo di accessori consentiti.
     *
     * @param numeroAccessori Il numero massimo di accessori da impostare.
     */
    public void setNumeroAccessori(int numeroAccessori) {
        this.numeroAccessori = numeroAccessori;
    }

    /**
     * Restituisce il prezzo del veicolo.
     *
     * @return Il prezzo del veicolo.
     */
    public double getPrezzo() {
        return prezzo;
    }

    /**
     * Imposta il prezzo del veicolo.
     *
     * @param prezzo Il prezzo del veicolo da impostare.
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    /**
     * Aggiunge un accessorio al veicolo.
     *
     * @param accessorio L'accessorio da aggiungere.
     * @return True se l'accessorio è stato aggiunto con successo, altrimenti False.
     */
    public boolean aggiungiAccessorio(Accessorio accessorio) {
        if (accessorio != null && this.accessori.size() < this.numeroAccessori) {
            System.out.println("Accessorio aggiunto al veicolo");
            return this.accessori.add(accessorio);
        }
        System.out.println("Numero massimo di accessori raggiunto, non è possibile aggiungerne altri");
        return false;
    }

    /**
     * Restituisce un array di accessori del veicolo.
     *
     * @return Un array di accessori del veicolo.
     */
    public Accessorio[] getAccessoriArray() {
        Accessorio[] accessoriArray = new Accessorio[this.accessori.size()];
        for (int i = 0; i < this.accessori.size(); i++) {
            accessoriArray[i] = this.accessori.get(i);
        }
        return accessoriArray;
    }

    /**
     * Calcola la velocita massima del veicolo in base alla potenza del motore.
     *
     * @return La velocita massima del veicolo.
     */
    public double calcolaVelocitaMassima() {
        if (getPotenzaMotore() < 200) {
            return (this.getPotenzaMotore() * 10) / 7;
        } else if (getPotenzaMotore() >= 250 && getPotenzaMotore() <= 300) {
            return (this.getPotenzaMotore() * 10) / 12;
        } else {
            return (this.getPotenzaMotore() * 10) / 22;
        }
    }

    /**
     * Stampa i nomi degli accessori del veicolo.
     */
    public void stampaAccessori() {
        for (Accessorio accessorio : this.accessori) {
            System.out.println(accessorio.getNomeAccessorio());
        }
    }

    /**
     * Calcola il costo totale del veicolo, inclusi gli accessori.
     *
     * @return Il costo totale del veicolo.
     */
    public double calcolaCostoTotale() {
        double totaleAccessori = 0;
        for (Accessorio accessorio : this.accessori) {
            totaleAccessori += accessorio.getPrezzo();
        }
        return this.prezzo + totaleAccessori;
    }


    /**
     * Questo metodo restituisce la quantita di carbutante impiegata per la distanza indicata tenendo presente del consumo medio
     * @param distanza distanza percorsa
     * @param consumoMedio consumo medio di carburante
     * @return
     */
    public double simulaViaggio(double distanza, double consumoMedio){
        return distanza / consumoMedio;
    }

    public double calcolaVelocitaMedia(){
        return Math.random()*100;
    }
}


