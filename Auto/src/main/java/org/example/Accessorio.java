package org.example;
/**
 * Questa classe rappresenta un accessorio per veicoli.
 */
public class Accessorio {
    private String nomeAccessorio;
    private double prezzo;

    /**
     * Costruttore per la classe Accessorio.
     *
     * @param nomeAccessorio Il nome dell'accessorio.
     * @param prezzo         Il prezzo dell'accessorio.
     */
    public Accessorio(String nomeAccessorio, double prezzo) {
        this.nomeAccessorio = nomeAccessorio;
        this.prezzo = prezzo;
    }

    /**
     * Restituisce il nome dell'accessorio.
     *
     * @return Il nome dell'accessorio.
     */
    public String getNomeAccessorio() {
        return nomeAccessorio;
    }

    /**
     * Imposta il nome dell'accessorio.
     *
     * @param nomeAccessorio Il nome dell'accessorio da impostare.
     */
    public void setNomeAccessorio(String nomeAccessorio) {
        this.nomeAccessorio = nomeAccessorio;
    }

    /**
     * Restituisce il prezzo dell'accessorio.
     *
     * @return Il prezzo dell'accessorio.
     */
    public double getPrezzo() {
        return prezzo;
    }

    /**
     * Imposta il prezzo dell'accessorio.
     *
     * @param prezzo Il prezzo dell'accessorio da impostare.
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
}