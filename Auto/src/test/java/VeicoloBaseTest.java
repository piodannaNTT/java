import org.example.Accessorio;
import org.example.VeicoloBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VeicoloBaseTest {

    VeicoloBase veicoloBase;
    Accessorio accessorio1 = new Accessorio("Fari Laser", 3500);
    Accessorio accessorio2 = new Accessorio("Pacchetto M-sport", 1750);
    Accessorio accessorio3 = new Accessorio("Sedili elettrici", 750);

    /**
     * Metodo utilizzato in fare in modo che all'esecuzione di ogni test vengano reinizializzate
     * le varie istanze di VeicoloBase e i suoi accessori, che poi verranno usati dai vari metodi di test
     * in modo da ripristinare sempre la situazione originale
     */
    @BeforeEach
     void setUp() {
        veicoloBase = new VeicoloBase("Bmw 320", 250, 5, 35000);
        veicoloBase.aggiungiAccessorio(accessorio1);
        veicoloBase.aggiungiAccessorio(accessorio2);
        veicoloBase.aggiungiAccessorio(accessorio3);

    }

    /**
     * Test affettuato sui valori di velocita massima che il veicolo raggiunge in
     * base alla potenza del motore
     */
    @Test
    void calcolaVelocitaMassima() {
        assertNotEquals(0, veicoloBase.calcolaVelocitaMassima());
        for(int i = 100; i < 130; i++){
            assertNotEquals(i, veicoloBase.calcolaVelocitaMassima());
        }
        if(veicoloBase.getPotenzaMotore() < 400){
            for(int i = 300; i < 330; i++){
                assertNotEquals(i, veicoloBase.calcolaVelocitaMassima());
            }
        }
    }

    /**
     * Test effettuato sul risultato dei costoTotale del veicolo calcolato tra prezzo dello stesso
     * e prezzo dei vari accessori
     */
    @Test
    void calcolaCostoTotale() {
        assertNotEquals(0, veicoloBase.calcolaCostoTotale());
        assertNotEquals(veicoloBase.getPrezzo(), veicoloBase.calcolaCostoTotale());
    }
}