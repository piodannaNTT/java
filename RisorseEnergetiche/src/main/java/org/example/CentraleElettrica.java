package org.example;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class CentraleElettrica {
    private RisorsaEnergetica[] risorseEnergetiche;
    private int numRisorse;
    private double efficienza;

    public CentraleElettrica(double efficienza) {
        this.risorseEnergetiche = new RisorsaEnergetica[5];
        this.numRisorse = 0;
        this.efficienza = efficienza;
    }

    public boolean aggiungiRisorsa(RisorsaEnergetica risorsaEnergetica){
        if(risorsaEnergetica != null && this.numRisorse < this.risorseEnergetiche.length){
            this.risorseEnergetiche[numRisorse] = risorsaEnergetica;
            this.numRisorse++;
            return true;
        }
        return false;
    }

    public void visualizzaRisorse(){
        for(int i = 0; i < this.numRisorse; i++){
            RisorsaEnergetica risorsaEnergetica = this.risorseEnergetiche[i];
            System.out.println("Risorsa: "+risorsaEnergetica.getNome()+
                    "\n\tQuanitità disponibile: "+risorsaEnergetica.getQuantitaDisponibile()+
                    "\n\tPotenza calorifica: "+risorsaEnergetica.getPotenzaCalorifuca()+
                    "\n\tPrezzo: "+risorsaEnergetica.getPrezzo()+"\n------------------");
        }
    }

    public double calcolaProduzioneEnergetica(){
        double potenzaTotale = 0;
        for(int i = 0; i < this.numRisorse; i++){
            RisorsaEnergetica risorsaEnergetica = this.risorseEnergetiche[i];
            potenzaTotale += (risorsaEnergetica.getPotenzaCalorifuca()  * risorsaEnergetica.getQuantitaDisponibile() * this.efficienza);
        }
        return potenzaTotale;
    }

    public void simulaConsumo(double consumo){
        double calcoloEnergiaElemento = 0;
        double calcoloEnergiaTotale = 0;
        double temp = 0;
        for(int i = 0; i < this.numRisorse; i++) {
            RisorsaEnergetica risorsaEnergetica = this.risorseEnergetiche[i];
            double maxDisp = risorsaEnergetica.getQuantitaDisponibile();
            for(int j = 0; j < maxDisp; j++){
                temp = (risorsaEnergetica.getPotenzaCalorifuca()  * j * this.efficienza);
                if(temp >= consumo){
                    this.risorseEnergetiche[i].setQuantitaDisponibile();
                    calcoloEnergiaTotale = temp;
                }
                else
            }

        }
    }
}
