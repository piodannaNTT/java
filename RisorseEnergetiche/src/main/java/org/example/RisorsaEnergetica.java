package org.example;

public class RisorsaEnergetica {
    public enum NomeRisorsa{
        Petrolio,
        Gas,
        Metano,
        Carbone,
        Uranio
    }
    private RisorsaEnergetica nome;
    private double quantitaDisponibile;
    private double potenzaCalorifuca;
    private double prezzo;

    public RisorsaEnergetica(RisorsaEnergetica nome, double quantitaDisponibile, double potenzaCalorifuca, double prezzo) {
        this.nome = nome;
        this.quantitaDisponibile = quantitaDisponibile;
        this.potenzaCalorifuca = potenzaCalorifuca;
        this.prezzo = prezzo;
    }

    public RisorsaEnergetica getNome() {
        return nome;
    }

    public double getQuantitaDisponibile() {
        return quantitaDisponibile;
    }

    public double getPotenzaCalorifuca() {
        return potenzaCalorifuca;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setQuantitaDisponibile(double quantitaDisponibile) {
        this.quantitaDisponibile = quantitaDisponibile;
    }

    public double calcolaCosto(double quantita){
        return quantita * this.prezzo;
    }
}
